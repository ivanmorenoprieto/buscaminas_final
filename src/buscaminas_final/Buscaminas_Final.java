/**
 *
 * @author Iván Moreno Prieto
 */

package buscaminas_final;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class Buscaminas_Final extends JFrame implements MouseListener {

    //Declaración de un tablero, y un JFrame llamdo buscMinas;
    Tablero tablero;
    static Buscaminas_Final buscMinas;

    //Aquí defino el menú que tendrá mi JFrame con sus opciones;
    private JMenuBar barraMenu;
    private JMenu opciones;
    private JMenuItem reiniciar;
    private JMenuItem cerrar;
    private JMenuItem cargarPartida;
    private JMenuItem guardarPartida;

    //Método main donde inicializaré el Jframe con su contenido y lo haré visible
    public static void main(String[] args) throws IOException {
        buscMinas = new Buscaminas_Final();
        buscMinas.setVisible(true);

    }

    //Constructor de la clase Buscamminas_Final(clase principal). Aquí llamo al método initBuscaminas, que inicia 
    //la parte del tablero en un JPanel, además indicamos el tamaño, llamamos al metodo crearMenu para añadir el menú al JFrame,
    //con pack hacemos que todos los componentes se coloquen/acoplen como es debido, hacemos qeu no se pueda redimensionar,
    //y colocamos un closeOperation para que el programa se cierre al darle a la x del JFrame. 
    public Buscaminas_Final() throws IOException { //Constructor del fram con el tablero
        initBuscaminas();
        this.setSize(tablero.getPreferredSize());
        crearMenu();
        this.pack();
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    //Método para iniciar buscaminas, donde generamos un nuevo objeto tablero 
    //añadimos el mouselistener, y añadimos el tablero al frame y ajustamos cada 
    //componente con pack en su sitio
    private void initBuscaminas() throws IOException {
        tablero = new Tablero();
        tablero.addMouseListener(this);
        this.getContentPane().add(tablero);
        this.pack();
    }

    //Metodo para crear el menú, crear los nuevos objetos, primero la barra de menú,
    //después el menú opciones, dentro de el iran los items reiniciar partida, cargar
    //partida reiniciar, y cerrar y le añado el nombre que aparecerá en cada uno.
    //a cada item dentro del menu opciones, le coloco un actionlistenes, y dentro un action perdormed
    //para añadir una acción que será un método que genero más abajo.
    //Todo el menú se monta añadiendo el menu de opciones a la menuBar y los items al menu opciones.
    private void crearMenu() {
        barraMenu = new JMenuBar();
        opciones = new JMenu("Opciones");
        reiniciar = new JMenuItem("Reiniciar Partida");
        reiniciar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    reiniciarBuscaminas();
                    JOptionPane.showMessageDialog(null, "¡PARTIDA REINICIADA!");
                } catch (IOException ex) {
                    Logger.getLogger(Buscaminas_Final.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        cargarPartida = new JMenuItem("Cargar Partida");
        cargarPartida.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    cargarPartida();
                } catch (IOException ex) {
                    Logger.getLogger(Buscaminas_Final.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Buscaminas_Final.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        guardarPartida = new JMenuItem("Guardar Partida");
        guardarPartida.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    guardarPartida();
                } catch (IOException ex) {
                    Logger.getLogger(Buscaminas_Final.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        cerrar = new JMenuItem("Cerrar Partida");
        cerrar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cerrarBuscaminas();
            }
        });
        opciones.add(cargarPartida);
        opciones.add(guardarPartida);
        opciones.add(reiniciar);
        opciones.add(cerrar);
        barraMenu.add(opciones);
        setJMenuBar(barraMenu);
    }

    //Método guardarPartida, aqui genero un ObjectOutputSream y le paso por parametro un 
    //FileOutputStream que a su vez le paso el nombre del fichero que generará que será donde guardará los atributos del tablero que necesito
    //Después de guardo con writeObject el tabCasilla que es mi array de casillas del tablero, y luego el boolean derrota, y los int
    //minas, y casillas marcadas, finalmente cierro el stream, e indico con un JOptionPane que la partida ha sido guardad.
    private void guardarPartida() throws FileNotFoundException, IOException {

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("guardarPartida.dat"));
        oos.writeObject(tablero.getTabCasilla());
        oos.writeBoolean(tablero.derrota);
        oos.writeInt(tablero.getMinas());
        oos.writeInt(tablero.getMarcadas());
        oos.close();
        JOptionPane.showMessageDialog(null, "¡PARTIDA GUARDADA!");

    }

    //Método para cargar la partida guardad, lo que hago es recuperar la última partida que se ha guardado, 
    //instancio un Objeto ObjectInputStream y le paso por parametro un FileInputStream que a su vez le paso 
    //el fichero que deberá leer.
    //Indico los atributos que sé que guarda el metodo guardar partida, en este caso tabCasilla, derrota, minas y marcadas,
    //ahi que leerlo en el orden que se ha escrito en el fichero.
    //el primero indico el tabCasilla de mi tablero actual cogerá los nuevos valores de la lectura, haciendo un casting
    //de la clase Casilla como array y readObject.
    //los demás atributos serán más sencillos ya que ObjectInputStream nos deja leer directamente el tipo de dato.
    //después de cambiarle los valores a los atributos, hago un repaint del Jpanel para que muestre los cambios,
    //finalmente cierro el stream de lectura, e indico con un JOptionPane que la partida ha sido recuperada.
    private void cargarPartida() throws FileNotFoundException, IOException, ClassNotFoundException {

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("guardarPartida.dat"));

        tablero.tabCasilla = (Casilla[][]) ois.readObject();
        tablero.derrota = ois.readBoolean();
        tablero.minas = ois.readInt();
        tablero.marcadas = ois.readInt(); //Para leer el 
        tablero.repaint();
        ois.close();
        JOptionPane.showMessageDialog(null, "¡PARTIDA CARGADA!");
    }

    //Método para reiniciar la partida, aqui simplemente con el getContentPane().remove elimino 
    //del JFrame el tablero actual y luego le paso de nuevo un método initBuscaminas() para generar un 
    //tablero nuevo con los atributos reiniciados.
    private void reiniciarBuscaminas() throws IOException { //método para reiniciar el buscaminas
        this.getContentPane().remove(tablero);
        initBuscaminas();
    }

    //Método para cerrar el buscaminas sencillo
    private void cerrarBuscaminas() {
        System.exit(0);
    }

    //Metodos para añadir al mouselistener, sólo uso el mouseReleased, 
    //en este caso lo que hago es que si el boton con el que clico es el izquierdo, 
    //previamente inicializando una variable x,y para luego decirle que la x e y será 
    //la posición donde clica el ratón, y entonces llamará al metodo de tablero.destapar esa 
    //posición en concreto, luego hago un repaint para que me muestre las imagenes correctas,
    //y si es un clic con el boton derecho, igual que con el boton izquierdo recibe donde ha clicado
    //y marca con una bandera la posicion, despues repinto el tablero.
    //Por último, añado un if else if, para indicar que si ganamos la partida aparecerá el mensaje
    //de que hemos ganado si no, si el tablero tiene la derrota a true aparecerá el mensaje de que hemos
    //perdido.
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        int x = 0, y = 0, i, j;
        if (e.getButton() == MouseEvent.BUTTON1) {
            x = e.getX();
            y = e.getY();
            tablero.destapar(x, y);
            tablero.repaint();

        } else if (e.getButton() == MouseEvent.BUTTON3) {
            x = e.getX();
            y = e.getY();
            tablero.marcar(x, y);
            tablero.repaint();
        }

        if (tablero.victoria()) {
            JOptionPane.showMessageDialog(null, "¡HAS GANADO LA PARTIDA =D!");
        } else if (tablero.derrota) {
            JOptionPane.showMessageDialog(null, "¡HAS PERDIDO LA PARTIDA :( !");

        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
