/**
 *
 * @author Iván Moreno Prieto
 */
package buscaminas_final;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

//Clase Casilla serializada
public class Casilla implements Serializable {

    //defino los atributos destapada, marcada, numeroVecinos, img y rect, 
    //destapada sera un boolean para saber si esta destapada la casilla o no,
    //igual con marcada, pero para saber si tenemos marcada con una flag o no.
    //img es un topo BuefferedImage, estará desserializado ya que no es un tipo serializable y 
    //esto me dió problemas al estar serializado.
    //Finalmente un atributo rect de tipo Rectangle2D.Float para crear los rectangulos por cada casilla
    private Boolean destapada, marcada;
    private int numeroVecinos;         //identificador de la casilla
    private transient BufferedImage img;
    private Rectangle2D.Float rect;

    //Constructor de la casilla que recibe por parámetro un rectangúlo.
    //La casilla se construirá con el rectangulo que le pasamos por parámetro,
    //y con los atributos inicializados a false y cero, como si empezase una 
    //partida totalemnte nueva.
    public Casilla(Rectangle2D.Float rect) {
        this.rect = rect;
        destapada = false;
        marcada = false;
        numeroVecinos = 0;
    }

    //Un paint Component que le pasamos un Graphics por parametro,
    //Con los primeros if if else, si la casilla está marcada nos enseñará en la casilla
    //la imagen de una bandera, si no, si no está destapada ensñara una imagen del tablero tapada,
    //si no se cumple nada de lo anterior, tenemos un switch controlado por el número de vecinos, 
    //donde segun los vecinos, tendra una imagenes según sú numero, si es 9, ya que alrededor solo puede llegar a haber 8 casillas, 
    //la novena será una bomba.
    void paintComponent(Graphics g) throws IOException {

        if (marcada) {                   //con estos ifs pinto si esta tapada o no y la bandera
            img = Tablero.band;
        } else if (!destapada) {
            img = Tablero.top;

        } else {
            switch (numeroVecinos) {
                //Identificador de la casilla, puede señalar, si es nueve, 
                //es mina, y si no los numeros de alrededor.   
                case 0:
                    img = Tablero.m0;
                    break;
                case 1:
                    img = Tablero.m1;
                    break;
                case 2:
                    img = Tablero.m2;
                    break;
                case 3:
                    img = Tablero.m3;
                    break;
                case 4:
                    img = Tablero.m4;
                    break;
                case 5:
                    img = Tablero.m5;
                    break;
                case 6:
                    img = Tablero.m6;
                    break;
                case 7:
                    img = Tablero.m7;
                    break;
                case 8:
                    img = Tablero.m8;
                    break;
                case 9:
                    img = Tablero.bomba;
                    break;

            }
        }
        //Con este método drawimage, pintamos cada rectangulo con su imagen.
        g.drawImage(img, (int) this.rect.x, (int) this.rect.y, null);
    }

    //Método que me devuelve destapada true
    public boolean destapada() {
        return destapada;

    }

    //Método para destapar la casilla, si no está marcada, 
    //destapada es verdadero
    public void destapar() {
        if (!marcada) {
            destapada = true;
        }
    }

    //Método para marcar con bandera una casilla
    public void marcar() {
        marcada = !marcada;
    }

    //Método que uso para definir si una casilla está marcada
    public boolean esMarcada() {
        return marcada;
    }

    //Método que me indica que si hay mina retorna true (9)
    public boolean hayMina() {
        return (this.numeroVecinos == 9);
    }

    //Método para plantar mina, lo que hago es que le indico una de las casillas
    //es el número 9
    public void plantarMina() {
        this.numeroVecinos = 9;
    }

    //Getters, setters y método to String
    public int getMinaVecino() {
        return (numeroVecinos);
    }

    public Boolean getMarcada() {
        return marcada;
    }

    public BufferedImage getImg() {
        return img;
    }

    public Rectangle2D.Float getRect() {
        return rect;
    }

    public Boolean getTapada() {
        return destapada;
    }

    public int getnumeroVecinos() {
        return numeroVecinos;
    }

    public void setTapada(Boolean tapada) {
        this.destapada = destapada;
    }

    public void setnumeroVecinos(int numeroVecinos) {
        this.numeroVecinos = numeroVecinos;
    }

    public void setMinaVecino(int minaVecino) {
        if (numeroVecinos != 9) {
            numeroVecinos = minaVecino;
        }

    }

    public void setMarcada(Boolean marcada) {
        this.marcada = marcada;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public void setRect(Rectangle2D.Float rect) {
        this.rect = rect;
    }

    @Override
    public String toString() {
        return "Casilla{" + "destapada=" + destapada + ", marcada=" + marcada + ", numeroVecinos=" + numeroVecinos + ", img=" + img + ", rect=" + rect + '}';
    }

}
