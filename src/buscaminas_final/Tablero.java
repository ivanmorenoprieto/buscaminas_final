/**
 *
 * @author Iván Moreno Prieto
 */

package buscaminas_final;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

//Clase Tablero el cual es un JPanel y está serializo la clase
public class Tablero extends JPanel implements Serializable {

    //Defino atributos tipo BufferedImage para mis imagenes.
    //Atributo numCasillas para el alto y ancho en cantidad de casillas utilizo solo una variable ya que es cuadrada
    //Atributo tamLado para el tamaño casilla por lado.
    //Atributo tabCasilla es el array de casillas.
    //Atributo para el número de minas en el tablero.
    //Atributo tipo boolean que determina la derrota.
    //Atributo tipo int marcadas para controlar cuantas casillas tengo marcadas con bandera.
    public static transient volatile BufferedImage top, m0, m1, m2, m3, m4, m5, m6, m7, m8, bomba, band; 
    private static final int numCasillas = 9; 
    private static final int tamLado = 50;  
    public Casilla tabCasilla[][]; 
    public int minas = 10; 
    public boolean derrota = false; 
    public int marcadas = 0; 

    //Constructor del tablero, con el metodo que crea el tablero, coloca las minas,
    //y actualiza los vecinos de las casillas.
    public Tablero() throws IOException {
        crearTablero();
        colocarMinas();
        actualizarVecinos();

    }

    //Genera las dimensiones de Tablero(con todas sus casillas).
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(numCasillas * tamLado, numCasillas * tamLado); 
    }

    //Este método crea el tablero, primero llamo al metodo imagenes tablero, construyo un objecto tabCasilla.
    //Recorro el array, y voy creando un rectangulo de la medida que necesito y luego lo asigno a cada casilla del array.
    public void crearTablero() throws IOException {
        imagenesTablero();
        tabCasilla = new Casilla[numCasillas][numCasillas]; 
        int y = 0;                                          
        for (int i = 0; i < numCasillas; i++) {
            int x = 0;
            for (int j = 0; j < numCasillas; j++) {
                Rectangle2D.Float rectang = new Rectangle2D.Float(x, y, tamLado, tamLado);
                tabCasilla[i][j] = new Casilla(rectang);
                x += tamLado;
            }
            y += tamLado;
        }

    }

    //Método de java para pintar los componentes que necesito recorriendo mi array de casillas   
    @Override
    public void paintComponent(Graphics g) {
        g.setColor(new Color(137, 165, 195));
        g.fillRect(0, 0, tamLado * numCasillas, tamLado * numCasillas);

        for (int i = 0; i < numCasillas; i++) {
            for (int j = 0; j < numCasillas; j++) {
                try {
                    tabCasilla[i][j].paintComponent(g);
                } catch (IOException ex) {
                    Logger.getLogger(Tablero.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    //coloco las minas con un random, mientras las minas sean > que 0,
    //asigno a las variables ranX y ranY un numero random entre 0 y el numero de casillas,
    //y si en esas casillas concretas no hay mina, planto una mina, y resto la cantidad de minas que tengo que plantar
    //y resto una mina para que acabe el bucle cuando llegue al numero de minas definido.
    public void colocarMinas() {             
        Random ran = new Random();          
        int ranX, ranY;                     

        while (minas > 0) {
            ranX = ran.nextInt(numCasillas);
            ranY = ran.nextInt(numCasillas);
            if (!(tabCasilla[ranX][ranY].hayMina())) {
                tabCasilla[ranX][ranY].plantarMina();
                minas--;
            }
        }
    }

    
    //Método que recorre todo el tablero, y para cada casilla revisa cuantas minas
    //hay alrededor suya, (en las 8 casillas). Y le añade con setMinVecino alos vecinos.
    public void actualizarVecinos() {
        int minasVecinos;
        for (int i = 0; i < numCasillas; i++) {

            for (int j = 0; j < numCasillas; j++) {

                minasVecinos = 0;
                for (int k = i - 1; k < i + 2; k++) {
                    for (int l = j - 1; l < j + 2; l++) {
                        //Este if es para que no se salga de los limites del tablero.
                        if (((k > -1) && (k < numCasillas))
                                && ((l > -1) && (l < numCasillas))
                                && (tabCasilla[k][l].hayMina())) {
                            minasVecinos++;

                        }
                    }
                }
                tabCasilla[i][j].setMinaVecino(minasVecinos);
            }
        }
    }

    //Método que uso para Destapar una casilla, 
    //recorro todo el array de casillas, a su vez, si la casilla contiene 
    //la posición que le paso por parametro y no esta marcada, destapa esa casilla, 
    //pero añado un if if else más, porque si hay mina, llamamos al metodo perderpartida para 
    //destaparlas todas, si no, llamamos al metodo getMinaVecino que sea igual a 0 y destapamos todos lo que tenemos a 0 cerca.
    public void destapar(int x, int y) {
        for (int i = 0; i < numCasillas; i++) {
            for (int j = 0; j < numCasillas; j++) {
                if ((tabCasilla[i][j].getRect().contains(x, y)) && (!tabCasilla[i][j].esMarcada())) {
                    tabCasilla[i][j].destapar();
                    if (tabCasilla[i][j].hayMina()) {
                        perderPartida();
                    } else if (tabCasilla[i][j].getMinaVecino() == 0) {

                        destaparArea();
                    }
                }
            }
        }
    }

    //Método para perder la partida, primero derrota sería true, y recorro todo el array de casillas
    //y le paso el metodo destapar, para destapar todas las casillas.
    public void perderPartida() {
        derrota = true;
        for (int i = 0; i < numCasillas; i++) {
            for (int j = 0; j < numCasillas; j++) {
                tabCasilla[i][j].destapar();
            }
        }
    }

    //Inicializo una variable recorridoAcabado a false, y comienzo un bucle suponiendo que el recorrido no ha acabado,
    //y una vez dentro asumo que ha acabado, si por algun casual el recorrido no hubiera acabado más adelante se ponen en false,
    //con lo cual significaria que no acaba el recorrido.
    //Recorremos todo el array de casillas y las examinamos si tiene minas vecino con 0 si esta destapada y si no esta marcada,
    //entonces las destapa, y luego para cada casilla se mira el area de cada minas alrededor.
    public void destaparArea() {
        boolean recorridoAcabado = false;
        while (!recorridoAcabado) {
            recorridoAcabado = true;
            for (int k = 0; k < numCasillas; k++) {
                for (int l = 0; l < numCasillas; l++) {
                    if ((tabCasilla[k][l].getMinaVecino() == 0) && (tabCasilla[k][l].destapada()) && (!tabCasilla[k][l].esMarcada())) {
                        for (int m = k - 1; m < k + 2; m++) {      //Los -1 los añado para que no se salga del borde.
                            for (int n = l - 1; n < l + 2; n++) {
                                if (((m > -1) && (m < numCasillas)) 
                                        && ((n > -1) && (n < numCasillas)
                                        && (!tabCasilla[m][n].destapada() && (!tabCasilla[m][n].esMarcada())))) {
                                    tabCasilla[m][n].destapar();
                                    recorridoAcabado = false;
                                }
                            }
                        }

                    }
                }

            }
        }
    }

    //Con este método recorro el tablero de casillas y si no esta destapada la marco y
    //Además añado un if más para controlar las marcadas con mina y restar si se desmarca.
    public void marcar(int x, int y) {
        for (int i = 0; i < numCasillas; i++) {
            for (int j = 0; j < numCasillas; j++) {
                if (tabCasilla[i][j].getRect().contains(x, y) && (!tabCasilla[i][j].destapada())) {
                    if ((!tabCasilla[i][j].esMarcada()) && (marcadas < 10)) {
                        tabCasilla[i][j].marcar();
                        marcadas++;

                    } else if (tabCasilla[i][j].esMarcada()) {
                        tabCasilla[i][j].marcar();
                        marcadas--;
                    }
                }
            }
        }
    }

    //Método boolean victoria, donde recorremos el array de casillas, y partimos
    //de que la victoria es true, si hay mina y no esta marcada no hemos perdido, 
    //por lo tanto retornara true;
    public boolean victoria() {
        boolean victoria = true;
        for (int i = 0; i < numCasillas; i++) {
            for (int j = 0; j < numCasillas; j++) {
                if ((tabCasilla[i][j].hayMina()) && (!tabCasilla[i][j].esMarcada())) {
                    victoria = false;
                }
            }
        }
        return victoria;

    }

    //En este método inicializo cada imágen con el método reescalarImagen, 
    //a su vez pasando por parámetro un ImageIO.read para leer el fichero con un new File, 
    //pasando por parametro la ruta de las imagenes.
    private void imagenesTablero() throws IOException {
        top = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/top.png")));
        bomba = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m9.png")));
        band = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/flag.png")));
        m0 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m0.png")));
        m1 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m1.png")));
        m2 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m2.png")));
        m3 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m3.png")));
        m4 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m4.png")));
        m5 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m5.png")));
        m6 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m6.png")));
        m7 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m7.png")));
        m8 = reescalarImagen(ImageIO.read(new File("src/buscaminas_final/img/m8.png")));
    }

    //Método de tipo BufferedImage para redimensionar la imagen a la medida de cada casilla
    private BufferedImage reescalarImagen(BufferedImage img) {
        int reescalado = Tablero.tamLado;
        int w = img.getWidth();
        int h = img.getHeight();
        BufferedImage bufim = new BufferedImage(reescalado, reescalado, img.getType());
        Graphics2D g2d = bufim.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.drawImage(img, 0, 0, reescalado, reescalado, 0, 0, w, h, null);
        g2d.dispose();
        return bufim;
    }

    //Getters, setters y método toString
    public static BufferedImage getBomba() {
        return bomba;
    }

    public static BufferedImage getBand() {
        return band;
    }

    public static int getNumCasillas() {
        return numCasillas;
    }

    public static int getTamLado() {
        return tamLado;
    }

    public Casilla[][] getTabCasilla() {
        return tabCasilla;
    }

    public int getMinas() {
        return minas;
    }

    public boolean isDerrota() {
        return derrota;
    }

    public int getMarcadas() {
        return marcadas;
    }

    public void setTabCasilla(Casilla[][] tabCasilla) {
        this.tabCasilla = tabCasilla;
    }

    public void setDerrota(boolean derrota) {
        this.derrota = derrota;
    }

    public void setMarcadas(int marcadas) {
        this.marcadas = marcadas;
    }

    @Override
    public String toString() {
        return "Tablero{" + "tabCasilla=" + tabCasilla + ", derrota=" + derrota + ", marcadas=" + marcadas + '}';
    }

}
