Proyecto:

Buscaminas para Programación2 de UIB.

Requisitos para usar en Visual Studio Code:

- Añadir el plugin: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack
- Instalar los siguientes JDK de java: https://www.oracle.com/es/java/technologies/javase/javase-jdk8-downloads.html // https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
- Configurar VSC: en VSC pulsamos "Ctrl+," en el buscador escribimos "java.home" y editamos el fichero añadiendo 
  la siguiente config:

```{
    "java.home": "/usr/lib/jvm/jdk-11.0.8",
    "java.configuration.runtimes": [
        {
          "name": "JavaSE-1.8",
          "path": "/usr/local/jdk1.8.0_111",
        },
        {
          "name": "JavaSE-11",
          "path": "/usr/lib/jvm/jdk-11.0.8",
          "default": true
        },
    ],
    "editor.suggestSelection": "first",
    "vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue"
}
```

Una vez hecho esto podremos abrir la carpeta que contiene el proyecto con VSC y empezar a probarlo apretando "Ctrl+F5".